Cinc Forquilles
========================

Demo app with the following requirements:

 * User registration
 * An scraper for a couple of websites
 * Search page

1) Installing the app
----------------------------------

Like a Symfony standard edition web app use composer to install dependencies.

### Use Composer

Just run the following command:

    php composer.phar update

Composer will install Symfony and all its dependencies.

2) Create schema and load fixtures
-------------------------------------

Simply run this command to create the schema:

    php app/console doctrine:schema:drop --force
    php app/console doctrine:schema:update --force

And for loading some fixtures:

    php app/console doctrine:fixtures:load PrunaticScraperBundle
    php app/console doctrine:fixtures:load PrunaticWebBundle


3) Running tests
-------------------------------------

I implemented some acceptance tests using Behat as a tool. You can read them on folder src/PrunaticWebBundle/Features. To run the acceptance tests please use the command:

    php bin/behat --config app/config/behat.yml @PrunaticWebBundle

Additionally there some functional and unit tests that may be run using phpunit as following:

    phpunit -c app/
