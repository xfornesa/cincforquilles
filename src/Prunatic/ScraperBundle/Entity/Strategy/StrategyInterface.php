<?php

namespace Prunatic\ScraperBundle\Entity\Strategy;

use Doctrine\Common\Collections\ArrayCollection;
use Goutte\Client;

interface StrategyInterface
{
    /**
     * Retrieve categories from a given html content
     *
     * @param $html
     * @return ArrayCollection
     */
    public function retrieveCategories($html);

    /**
     * Retrieve products from a given html content
     *
     * @param $html
     * @param ArrayCollection $categories
     * @return ArrayCollection
     */
    public function retrieveProducts($html, ArrayCollection $categories);

    /**
     * Set Http Client to be used
     *
     * @param Client $client
     * @return mixed
     */
    public function setHttpClient(Client $client);
}
