<?php
/**
 * Created by JetBrains PhpStorm.
 * User: xavier
 * Date: 20/10/13
 * Time: 10:53
 * To change this template use File | Settings | File Templates.
 */

namespace Prunatic\ScraperBundle\Entity\Strategy;


class MissingHttpClientException extends \Exception
{
    protected $message = 'HTTP Client missing';
}
