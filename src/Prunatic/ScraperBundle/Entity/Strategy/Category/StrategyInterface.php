<?php

namespace Prunatic\ScraperBundle\Entity\Strategy\Category;

use Doctrine\Common\Collections\ArrayCollection;

interface StrategyInterface
{
    /**
     * Retrieve categories from a given html content
     *
     * @param $html
     * @return ArrayCollection
     */
    public function retrieveCategories($html);
}
