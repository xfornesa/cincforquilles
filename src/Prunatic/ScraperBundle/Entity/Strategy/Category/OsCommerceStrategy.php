<?php

namespace Prunatic\ScraperBundle\Entity\Strategy\Category;

use Doctrine\Common\Collections\ArrayCollection;
use Prunatic\ScraperBundle\Entity\Category;
use Prunatic\ScraperBundle\Entity\ScraperHelper;
use Prunatic\ScraperBundle\Entity\Strategy\Category\StrategyInterface;
use Symfony\Component\DomCrawler\Crawler;

class OsCommerceStrategy implements StrategyInterface
{
    /**
     * {@inheritDoc}
     */
    public function retrieveCategories($html)
    {
        $categories = new ArrayCollection();
        $crawler = new Crawler($html);
        $filter = $crawler->filter('.suckerdiv > [id^="suckertree"] > li');
        foreach ($filter as $element) {
            $parentCategory = $this->extractCategoryDetail($element);
            $categories->add($parentCategory);
            $children = $this->retrieveSubCategories($element);
            foreach ($children as $child) {
                /** @var Category $child */
                $child->setParent($parentCategory);
                $categories->add($child);
            }
        }

        return $categories;
    }

    /**
     * @param $html
     * @return ArrayCollection
     */
    public function retrieveSubCategories($html)
    {
        $categories = new ArrayCollection();

        $crawler = new Crawler($html);
        $filter = $crawler->filter('[class^="selectedul"]');
        foreach ($filter as $element) {
            $category = $this->extractCategoryDetail($element);
            $categories->add($category);
        }

        return $categories;
    }

    /**
     * @param $element
     * @return Category
     */
    public function extractCategoryDetail($element)
    {
        $helper = new ScraperHelper();
        $crawler = new Crawler($element);
        $name = $crawler->filter('a')->first()->text();
        $name = $helper->cleanStringForTitle($name);
        $url = $crawler->filter('a')->first()->attr('href');

        $category = new Category();
        $category
            ->setName($name)
            ->setUrl($url);

        return $category;
    }
}
