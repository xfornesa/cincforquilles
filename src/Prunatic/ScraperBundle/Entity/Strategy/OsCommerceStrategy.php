<?php

namespace Prunatic\ScraperBundle\Entity\Strategy;

use Prunatic\ScraperBundle\Entity\Strategy\StrategyInterface;
use Prunatic\ScraperBundle\Entity\Strategy\Category\OsCommerceStrategy as CategoryStrategy;
use Prunatic\ScraperBundle\Entity\Strategy\Product\OsCommerceStrategy as ProductStrategy;

class OsCommerceStrategy extends AbstractStrategy implements StrategyInterface
{
    /**
     * @return CategoryStrategy
     */
    public function getCategoryStrategy()
    {
        $strategy = new CategoryStrategy();

        return $strategy;
    }

    /**
     * @return ProductStrategy
     */
    public function getProductStrategy()
    {
        $strategy = new ProductStrategy();

        return $strategy;
    }
}
