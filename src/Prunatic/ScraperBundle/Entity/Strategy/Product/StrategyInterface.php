<?php

namespace Prunatic\ScraperBundle\Entity\Strategy\Product;

use Doctrine\Common\Collections\ArrayCollection;
use Goutte\Client;

interface StrategyInterface
{
    /**
     * Retrieve products from a given html content
     *
     * @param string $html
     * @return ArrayCollection
     */
    public function retrieveProducts($html);

    /**
     * Set categories
     *
     * @param ArrayCollection $categories
     */
    public function setCategories(ArrayCollection $categories);

    /**
     * Set HTTP Client
     *
     * @param Client $client
     */
    public function setHttpClient(Client $client);
}
