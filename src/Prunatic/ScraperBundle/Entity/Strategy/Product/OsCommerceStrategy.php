<?php

namespace Prunatic\ScraperBundle\Entity\Strategy\Product;

use Doctrine\Common\Collections\ArrayCollection;
use Goutte\Client;
use Prunatic\ScraperBundle\Entity\Category;
use Prunatic\ScraperBundle\Entity\Product;
use Prunatic\ScraperBundle\Entity\ScraperHelper;
use Prunatic\ScraperBundle\Entity\Strategy\MissingHttpClientException;
use Prunatic\ScraperBundle\Entity\Strategy\Product\StrategyInterface;
use Symfony\Component\DomCrawler\Crawler;

class OsCommerceStrategy implements StrategyInterface
{
    /** @var ArrayCollection */
    protected $categories;

    protected $client;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    /**
     * Set categories
     *
     * @param ArrayCollection $categories
     * @return OsCommerceStrategy
     */
    public function setCategories(ArrayCollection $categories)
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * @param Client $client
     * @return $this
     */
    public function setHttpClient(Client $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function retrieveProducts($html)
    {
        if (empty($this->client)) {
            throw new MissingHttpClientException();
        }
        $products = new ArrayCollection();

        foreach ($this->categories as $category) {
            /** @var Category $category */
            $url = $category->getUrl();
            $crawler = $this->client->request('GET', $url);
            $html = $crawler->html();
            $productsInCategory = $this->extractProductsForCategory($html);
            foreach ($productsInCategory as $product) {
                $products->add($product);
            }
        }

        return $products;
    }

    public function extractProductsForCategory($html)
    {
        $products = new ArrayCollection();

        $urls = $this->extractFollowingCategoryUrls($html);
        foreach ($urls as $url) {
            $crawler = $this->client->request('GET', $url);
            $html = $crawler->html();
            $productsInCategory = $this->extractProductsFromList($html);
            foreach ($productsInCategory as $product) {
                $products->add($product);
            }
        }

        return $products;
    }

    public function extractFollowingCategoryUrls($html)
    {
        // TODO process pagination and return all urls for that category

        return array();
    }

    public function extractProductsFromList($html)
    {
        $products = new ArrayCollection();
        $urls = $this->extractProductDetailUrlsFromCategoryPage($html);
        foreach ($urls as $url) {
            $crawler = $this->client->request('GET', $url);
            $html = $crawler->html();
            $product = $this->extractProductDetail($html);
            $products->add($product);
        }

        return $products;
    }


    public function extractProductDetailUrlsFromCategoryPage($html)
    {
        // retrieve product excerpts and get product detail to manage

        return array();
    }

    /**
     * Retrieve Product from html content
     * @param $html
     * @return Product
     */
    public function extractProductDetail($html)
    {
        $product = new Product();
        $crawler = new Crawler();
        $crawler->addHtmlContent($html, 'UTF-8');

        $detailFilter = $crawler->filter('.separador_central > form[name="cart_quantity"]');

        $name = $this->extractName($detailFilter);
        $image = $this->extractImage($detailFilter);
        $description = $this->extractDescription($detailFilter);
        list($price, $currency) = $this->extractPricing($detailFilter);

        $product->setName($name);
        $product->setImageUrl($image);
        $product->setDescription($description);
        $product->setPrice($price);
        $product->setCurrency($currency);

        return $product;
    }

    /**
     * @param Crawler $detailFilter
     * @return mixed
     */
    public function extractImage($detailFilter)
    {
        $image = $detailFilter->filter('.smallText a[href*=".jpg"]')->first()->attr('href');
        if (empty($image)) {
            $image = $detailFilter->filter('.smallText img[src^="product_thumb.php"]')->first()->attr('src');

            return $image;
        }

        return $image;
    }

    /**
     * @param Crawler $detailFilter
     * @return string
     */
    public function extractName($detailFilter)
    {
        $helper = new ScraperHelper();
        $name = $detailFilter->filter('h1')->first()->text();
        $name = $helper->cleanStringForTitle($name);

        return $name;
    }

    /**
     * @param Crawler $detailFilter
     * @return string
     */
    public function extractDescription($detailFilter)
    {
        $helper = new ScraperHelper();
        $descriptionLines = $detailFilter->filter('.main3 .main3 p')->each(
            function (Crawler $node, $i) use ($helper) {
                return $helper->cleanStringForTitle($node->text());
            }
        );
        $descriptionLines = array_filter($descriptionLines);
        $description = implode("\n", $descriptionLines);

        return $description;
    }

    /**
     * @param Crawler $detailFilter
     * @return array
     */
    public function extractPricing($detailFilter)
    {
        $price = null;
        $currency = null;

        $pricing = $detailFilter->filter('.fuente_precio_info')->text();
        $pricing = trim($pricing);
        // examples: PVP 20,50€
        $pricingPattern = '/(*UTF8)\D*\s*(\d+[\.\d+{3}]*[\,\d+{0,2}|?]*)\s*(\S*)\s*/i';
        if (preg_match($pricingPattern, $pricing, $matches)) {
            $price = $matches[1];
            $price = str_replace('.', '', $price);
            $price = str_replace(',', '.', $price);
            $price = (float) $price;
            $currency = $matches[2];
        }

        return array($price, $currency);
    }
}
