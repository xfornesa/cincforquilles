<?php

namespace Prunatic\ScraperBundle\Entity\Strategy;

use Doctrine\Common\Collections\ArrayCollection;
use Goutte\Client;
use Prunatic\ScraperBundle\Entity\Strategy\StrategyInterface;

abstract class AbstractStrategy implements StrategyInterface
{
    /** @var  Client */
    protected $client;

    /**
     * @param Client $client
     * @return AbstractStrategy
     */
    public function setHttpClient(Client $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function retrieveProducts($html, ArrayCollection $categories)
    {
        if (empty($this->client)) {
            throw new MissingHttpClientException();
        }
        $strategy = $this->getProductStrategy();
        $strategy->setCategories($categories);
        $strategy->setHttpClient($this->client);
        $products = $strategy->retrieveProducts($html);

        return $products;
    }

    /**
     * {@inheritDoc}
     */
    public function retrieveCategories($html)
    {
        $strategy = $this->getCategoryStrategy();
        $categories = $strategy->retrieveCategories($html);

        return $categories;
    }

    /**
     * @return \Prunatic\ScraperBundle\Entity\Strategy\Category\StrategyInterface
     */
    abstract public function getCategoryStrategy();

    /**
     * @return \Prunatic\ScraperBundle\Entity\Strategy\Product\StrategyInterface
     */
    abstract public function getProductStrategy();
}
