<?php

namespace Prunatic\ScraperBundle\Entity\Strategy;

class MissingStrategyException extends \Exception
{
    protected $message = 'Scraper strategy missing';
}
