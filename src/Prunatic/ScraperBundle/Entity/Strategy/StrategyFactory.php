<?php

namespace Prunatic\ScraperBundle\Entity\Strategy;

use Prunatic\ScraperBundle\Entity\ApplicationPlatform;
use Prunatic\ScraperBundle\Entity\Strategy\OsCommerceStrategy;
use Prunatic\ScraperBundle\Entity\Strategy\StrategyInterface;

class StrategyFactory
{
    /**
     * Retrieve an scraper strategy for that type of web application
     *
     * @param $application
     * @return null|StrategyInterface
     */
    public function createStrategy($application)
    {
        $strategy = null;
        switch($application) {
            case ApplicationPlatform::OSCOMMERCE:
                $strategy = new OsCommerceStrategy();
                break;
        }

        return $strategy;
    }
}
