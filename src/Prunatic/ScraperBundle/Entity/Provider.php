<?php

namespace Prunatic\ScraperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Prunatic\ScraperBundle\Entity\ApplicationPlatform;

/**
 * Provider
 *
 * @ORM\Table(name="provider")
 * @ORM\Entity
 */
class Provider
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text")
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="application_platform", columnDefinition="ENUM('oscommerce')", nullable=true)
     */
    private $applicationPlatform;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Provider
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Provider
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set applicationPlatform
     *
     * @param string $applicationPlatform
     * @throws NonValidApplicationPlatformException
     * @return Provider
     */
    public function setApplicationPlatform($applicationPlatform)
    {
        if (!$this->isApplicationPlatformValid($applicationPlatform)) {
            throw new NonValidApplicationPlatformException();
        }
        $this->applicationPlatform = $applicationPlatform;
    
        return $this;
    }

    /**
     * Returns if an application platform is implemented
     * @param $platform
     * @return bool
     */
    public function isApplicationPlatformValid($platform)
    {
        return in_array($platform, $this->getAvailableApplicationPlatforms());
    }

    /**
     * Retrieve all available application platform for any provider
     * @return array
     */
    public function getAvailableApplicationPlatforms()
    {
        return array(
            ApplicationPlatform::OSCOMMERCE
        );
    }

    /**
     * Get applicationPlatform
     *
     * @return string 
     */
    public function getApplicationPlatform()
    {
        return $this->applicationPlatform;
    }
}
