<?php

namespace Prunatic\ScraperBundle\Entity;

use Goutte\Client;
use Prunatic\ScraperBundle\Entity\Strategy\MissingHttpClientException;
use Prunatic\ScraperBundle\Entity\Strategy\MissingStrategyException;
use Prunatic\ScraperBundle\Entity\Strategy\StrategyInterface;

class Scraper
{
    /** @var StrategyInterface */
    protected $strategy;

    /** @var  Client */
    protected $client;

    public function __construct()
    {
        $this->strategy = null;
        $this->client = null;
    }

    /**
     * @param StrategyInterface $strategy
     */
    public function setStrategy($strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * Runs the scraper with given strategy
     *
     * @param string $url
     * @throws Strategy\MissingStrategyException
     * @throws Strategy\MissingHttpClientException
     */
    public function execute($url)
    {
        if (empty($this->strategy)) {
            throw new MissingStrategyException();
        }
        if (empty($this->client)) {
            throw new MissingHttpClientException();
        }
        $html = '';
        $categories = $this->strategy->retrieveCategories($html);
        $products = $this->strategy->retrieveProducts($html, $categories);
        // TODO persists objects found
    }

    /**
     * @param Client $client
     */
    public function setHttpClient($client)
    {
        $this->client = $client;
    }
}
