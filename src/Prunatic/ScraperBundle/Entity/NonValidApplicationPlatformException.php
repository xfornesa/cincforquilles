<?php
/**
 * Created by JetBrains PhpStorm.
 * User: xavier
 * Date: 19/10/13
 * Time: 11:26
 * To change this template use File | Settings | File Templates.
 */

namespace Prunatic\ScraperBundle\Entity;


class NonValidApplicationPlatformException extends \Exception
{
    protected $message = 'Application platform not implemented';
}
