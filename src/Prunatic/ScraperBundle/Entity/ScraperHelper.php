<?php

namespace Prunatic\ScraperBundle\Entity;


class ScraperHelper
{
    /**
     * Returns a clean string of separators (two many spaces, new lines, etc)
     * @param string $title
     * @return string
     */
    public function cleanStringForTitle($title)
    {
        return trim(preg_replace('/\s\s+/', ' ', $title));
    }
}
