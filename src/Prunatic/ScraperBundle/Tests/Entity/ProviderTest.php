<?php
/**
 * Created by JetBrains PhpStorm.
 * User: xavier
 * Date: 19/10/13
 * Time: 11:20
 * To change this template use File | Settings | File Templates.
 */

namespace Prunatic\ScraperBundle\Tests\Entity;


use Prunatic\ScraperBundle\Entity\Provider;

class ProviderTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Provider $SUT */
    protected $SUT;

    public function setUp()
    {
        $this->SUT = new Provider();
    }

    public function testApplicationPlatform()
    {
        $validPlatforms = $this->SUT->getAvailableApplicationPlatforms();
        $platform = reset($validPlatforms);
        $this->SUT->setApplicationPlatform($platform);
        $this->assertEquals($platform, $this->SUT->getApplicationPlatform());
    }

    /**
     * @expectedException \Prunatic\ScraperBundle\Entity\NonValidApplicationPlatformException
     */
    public function testTryingToSetNonValidApplicationPlatform()
    {
        $invalidPlatform = 'invalid-platform';
        $validPlatforms = $this->SUT->getAvailableApplicationPlatforms();
        $this->assertNotContains($invalidPlatform, $validPlatforms);
        $this->SUT->setApplicationPlatform($invalidPlatform);
    }
}
