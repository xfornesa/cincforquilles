<?php

namespace Prunatic\ScraperBundle\Tests\Entity\Strategy;

use Prunatic\ScraperBundle\Entity\Strategy\StrategyFactory;

class StrategyFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var StrategyFactory */
    protected $SUT;

    public function setUp()
    {
        $this->SUT = new StrategyFactory;
    }

    /**
     * @dataProvider getStrategyTypes
     * @param $application
     * @param $class
     */
    public function testStrategyTypes($application, $class)
    {
        $expectedObject = new $class();
        $strategy = $this->SUT->createStrategy($application);
        $this->assertInstanceOf(get_class($expectedObject), $strategy);
    }

    public function getStrategyTypes()
    {
        return array(
            array(
                'oscommerce', '\Prunatic\ScraperBundle\Entity\Strategy\OsCommerceStrategy'
            )
        );
    }

}
