<?php

namespace Prunatic\ScraperBundle\Tests\Entity\Strategy;

use Doctrine\Common\Collections\ArrayCollection;
use Goutte\Client;
use Prunatic\ScraperBundle\Entity\Strategy\AbstractStrategy;
use Prunatic\ScraperBundle\Entity\Strategy\Category\StrategyInterface as CategoryStrategyInterface;
use Prunatic\ScraperBundle\Entity\Strategy\Category\StrategyInterface as ProductStrategyInterface;

class AbstractStrategyTest extends \PHPUnit_Framework_TestCase
{
    /** @var AbstractStrategy */
    protected $SUT;

    /** @var CategoryStrategyInterface */
    protected $categoryStrategy;

    /** @var ProductStrategyInterface */
    protected $productStrategy;

    /** @var  Client */
    protected $client;

    public function setUp()
    {
        $this->SUT = $this->getMockForAbstractClass('\Prunatic\ScraperBundle\Entity\Strategy\AbstractStrategy');
        $this->categoryStrategy = $this->getMock('\Prunatic\ScraperBundle\Entity\Strategy\Category\StrategyInterface');
        $this->productStrategy = $this->getMock('\Prunatic\ScraperBundle\Entity\Strategy\Product\StrategyInterface');
        $this->client = $this->getMock('\Goutte\Client');
    }

    /**
     * @expectedException \Prunatic\ScraperBundle\Entity\Strategy\MissingHttpClientException
     */
    public function testExecuteWithoutHttpClient()
    {
        $this->SUT->retrieveProducts('', new ArrayCollection());
    }

    public function testRetrieveCategories()
    {
        $this->categoryStrategy
            ->expects($this->once())
            ->method('retrieveCategories')
            ->with($this->equalTo(''))
            ->will($this->returnValue(new ArrayCollection()));
        $this->SUT
            ->expects($this->once())
            ->method('getCategoryStrategy')
            ->will($this->returnValue($this->categoryStrategy));
        $this->SUT->retrieveCategories('');
    }

    public function testRetrieveProducts()
    {
        $this->productStrategy
            ->expects($this->once())
            ->method('retrieveProducts')
            ->will($this->returnValue(new ArrayCollection()));

        $this->SUT
            ->expects($this->once())
            ->method('getProductStrategy')
            ->will($this->returnValue($this->productStrategy));

        $this->SUT->setHttpClient($this->client);
        $this->SUT->retrieveProducts('', new ArrayCollection());
    }
}
