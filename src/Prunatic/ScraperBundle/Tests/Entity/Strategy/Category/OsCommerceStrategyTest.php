<?php

namespace Prunatic\ScraperBundle\Tests\Entity\Strategy\Category;

use Doctrine\Common\Collections\ArrayCollection;
use Prunatic\ScraperBundle\Entity\Category;
use Prunatic\ScraperBundle\Entity\Strategy\Category\OsCommerceStrategy;

class OsCommerceStrategyTest extends \PHPUnit_Framework_TestCase
{
    /** @var  OsCommerceStrategy */
    protected $SUT;

    public function setUp()
    {
        $this->SUT = new OsCommerceStrategy();
    }

    /**
     * @dataProvider categoryProvider
     */
    public function testExtractCategories($example, $html, ArrayCollection $expectedCategories)
    {
        $categories = $this->SUT->retrieveCategories($html);

        $difference = array_udiff(
            $expectedCategories->toArray(),
            $categories->toArray(),
            function ($a, $b) {
                return strcmp($a->getName().$a->getUrl(), $b->getName().$b->getUrl());
            }
        );
        $this->assertEmpty($difference, "Fail example '$example' ");
    }

    public function categoryProvider()
    {
        $data = array();

        $data[] = $this->exampleEmptyContent();
        $data[] = $this->exampleOneDepthMenuOneElement();
        $data[] = $this->exampleOneDepthMenuTwoElements();
        $data[] = $this->exampleOneDepthMenuOneElementWithChildren();

        return $data;
    }

    private function exampleEmptyContent()
    {
        return array('EmptyContent', '', new ArrayCollection());
    }

    private function exampleOneDepthMenuOneElement()
    {
        $categories = new ArrayCollection();
        $category = $this->createCategory('Accesorios varios', '/Accesorios-varios.html');
        $categories->add($category);

        $html = <<<EOF
<html>
<head></head>
<body>
    <div class="suckerdiv">
        <ul id="suckertree1">
            <li><a href="/Accesorios-varios.html">Accesorios
                    varios</a></li>
        </ul>
    </div>
</body>
</html>
EOF;

        return array('OneDepthMenuOneElement', $html, $categories);
    }

    private function exampleOneDepthMenuTwoElements()
    {
        $categories = new ArrayCollection();
        $categoryA = $this->createCategory('Utensilios de cocina', '/Utensilios-de-cocina.html');
        $categoryB = $this->createCategory('Cocina', '/Cocina.html');
        $categories->add($categoryA);
        $categories->add($categoryB);

        $html = <<<EOF
<html>
<head></head>
<body>
    <div class="suckerdiv">
        <ul id="suckertree1">
            <li>
                <a href="/Utensilios-de-cocina.html"
                   class="subfolderstyle">Utensilios de cocina</a>
            </li>
            <li><a href="/Cocina.html"
                   class="subfolderstyle">Cocina</a>
            </li>
        </ul>
    </div>
</body>
</html>
EOF;

        return array('OneDepthMenuTwoElements', $html, $categories);
    }

    private function exampleOneDepthMenuOneElementWithChildren()
    {
        $categories = new ArrayCollection();
        $parent = $this->createCategory('Utensilios de cocina', '/Utensilios-de-cocina.html');
        $child = $this->createCategory('Mandolinas', '/mandolinas.html');
        $child->setParent($parent);
        $categories->add($parent);
        $categories->add($child);

        $html = <<<EOF
<html>
<head></head>
<body>
    <div class="suckerdiv">
        <ul id="suckertree1">
            <li>
                <a href="/Utensilios-de-cocina.html"
                   class="subfolderstyle">Utensilios de cocina</a>
                <ul class="selectedul1">
                    <li><a href="/mandolinas.html">Mandolinas</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</body>
</html>
EOF;

        return array('OneDepthMenuOneElementWithChildren', $html, $categories);
    }

    /**
     * @param string $name
     * @param string $url
     * @return Category
     */
    private function createCategory($name, $url)
    {
        $category = new Category();
        $category
            ->setName($name)
            ->setUrl($url);


        return $category;
    }

}
