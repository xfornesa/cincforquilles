<?php

namespace Prunatic\ScraperBundle\Tests\Entity\Strategy\Product;

use Doctrine\Common\Collections\ArrayCollection;
use Goutte\Client;
use Prunatic\ScraperBundle\Entity\Product;
use Prunatic\ScraperBundle\Entity\Strategy\Product\OsCommerceStrategy;

class OsCommerceStrategyTest extends \PHPUnit_Framework_TestCase
{
    /** @var  OsCommerceStrategy */
    protected $SUT;

    /** @var  Client */
    protected $client;

    public function setUp()
    {
        $this->SUT = new OsCommerceStrategy();
        $this->client = $this->getMock('\Goutte\Client');
    }

    /**
     * @expectedException \Prunatic\ScraperBundle\Entity\Strategy\MissingHttpClientException
     */
    public function testTryingToRetrieveProductsWithoutHttpClient()
    {
        $this->SUT->retrieveProducts('');
    }

    /**
     * @dataProvider providerProductDetail
     */
    public function testProductNameWhenExtractingProductDetail($html, $expectedProduct)
    {
        $product = $this->SUT->extractProductDetail($html);
        $this->assertEquals($expectedProduct->getName(), $product->getName());
    }

    /**
     * @dataProvider providerProductDetail
     */
    public function testProductImageWhenExtractingProductDetail($html, $expectedProduct)
    {
        $product = $this->SUT->extractProductDetail($html);
        $this->assertEquals($expectedProduct->getImageUrl(), $product->getImageUrl());
    }

    /**
     * @dataProvider providerProductDetail
     */
    public function testProductDescriptionWhenExtractingProductDetail($html, $expectedProduct)
    {
        $product = $this->SUT->extractProductDetail($html);
        $expectedDescription = $expectedProduct->getDescription();
        $description = $product->getDescription();
        $this->assertEquals($expectedDescription, $description);
    }

    /**
     * @dataProvider providerProductDetail
     */
    public function testProductPricingWhenExtractingProductDetail($html, $expectedProduct)
    {
        $product = $this->SUT->extractProductDetail($html);
        $expectedPricing = $expectedProduct->getPrice() . $expectedProduct->getCurrency();
        $pricing = $product->getPrice() . $product->getCurrency();
        $this->assertEquals($expectedPricing, $pricing);
    }

    public function providerProductDetail()
    {
        $product = new Product();
        $description = <<<EOF
CORTA PATATAS - 3 CUCHILLAS "WESTMARK"
CORTADORA DE PATATAS PARA FREIR Y VERDURAS. OFRECE MULTIPLES POSIBILIDADES. CORTA PATATAS PARA FREIR ASI COMO VERDURAS Y FRUTAS (p.ej. PARA ENSALADAS Y MACEDONIAS)
EOF;

        $product
            ->setName('Corta patatas 3 cuchillas')
            ->setImageUrl('http://www.losutensiliosdelchef.com/images/p_1174522363_CORTAPATATAS3CUCHILLAS.jpg')
            ->setDescription($description)
            ->setPrice(20.50)
            ->setCurrency('€');

        $html = <<<EOF
<html>
<head></head>
<body>
<div class="separador_central">
<form name="cart_quantity" action="http://www.losutensiliosdelchef.com/?cPath=CORTA-PATATAS-3-CUCHILLAS.html&amp;action=add_product" method="post">
<table border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td>
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td valign="top"><h1>Corta patatas 3 cuchillas</h1>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td align="center" class="smallText">
                                    <table border="0" cellspacing="0" cellpadding="6" align="center">
                                        <tbody>
                                        <tr>
                                            <td align="center" class="smallText" widht="140px">
                                                <a href="http://www.losutensiliosdelchef.com/images/p_1174522363_CORTAPATATAS3CUCHILLAS.jpg"
                                                   target="_blank" rel="lightbox[group]"
                                                   title="Corta patatas 3 cuchillas"><img
                                                            src="product_thumb.php?img=images/p_1174522363_CORTAPATATAS3CUCHILLAS.jpg&amp;w=130&amp;h=130"
                                                            width="130" height="130" hspace="5" vspace="5" border="0"
                                                            alt="Corta patatas 3 cuchillas"
                                                            title="Corta patatas 3 cuchillas"><br><img border="0"
                                                                                                       src="images/zoom.gif"></a>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" class="fondo_precio">
                            <tbody>
                            <tr>
                                <td align="center" valign="middle">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td class="fuente_precio_oferta_info" valign="middle"></td>
                                            <td width="25px">&nbsp;</td>
                                            <td class="fuente_precio_info">PVP <b>20,50€</b></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td class="main3">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr>
                    <td width="100%" class="main3" valign="top">
                        <p></p>

                        <p><font class="s"><b style="COLOR: black; BACKGROUND-COLOR: #ffff66">CORTA</b>
                                <b style="COLOR: black; BACKGROUND-COLOR: #a0ffff">PATATAS</b></font> -
                            <b style="COLOR: black; BACKGROUND-COLOR: #99ff99">3</b>
                            <b style="COLOR: black; BACKGROUND-COLOR: #ff9999">CUCHILLAS</b> "WESTMARK" </p>

                        <p>CORTADORA DE <b style="COLOR: black; BACKGROUND-COLOR: #a0ffff">PATATAS</b>
                            PARA FREIR Y VERDURAS. OFRECE MULTIPLES POSIBILIDADES.
                            <b style="COLOR: black; BACKGROUND-COLOR: #ffff66">CORTA</b>
                            <b style="COLOR: black; BACKGROUND-COLOR: #a0ffff">PATATAS</b> PARA FREIR ASI
                            COMO VERDURAS Y FRUTAS (p.ej. PARA ENSALADAS Y MACEDONIAS)</p>

                        <p></p>
                    </td>
                    <td valign="top" width="42%" align="right">

                    </td>
                    <td width="0%"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</form>
</div>
</body>
</html>
EOF;

        return array(
            array($html, $product)
        );
    }

    /**
     * @dataProvider productProvider
     */
    public function testExtractProducts($example, $html, ArrayCollection $expectedProducts, ArrayCollection $expectedCategories)
    {
        $this->SUT->setHttpClient($this->client);
        $this->SUT->setCategories($expectedCategories);
        $products = $this->SUT->retrieveProducts($html);

        $difference = array_udiff(
            $expectedProducts->toArray(),
            $products->toArray(),
            function ($a, $b) {
                return strcmp($a->getName().$a->getUrl(), $b->getName().$b->getUrl());
            }
        );
        $this->assertEmpty($difference, "Fail example '$example' ");
    }

    public function productProvider()
    {
        $data = array();

        $data[] = $this->exampleEmptyContent();

        return $data;
    }

    private function exampleEmptyContent()
    {
        return array('EmptyContent', '', new ArrayCollection(), new ArrayCollection());
    }

    /**
     * @dataProvider extractCategoryUrlsProvider
     */
    public function testsExtractCategoryUrls($html, $expectedUrls)
    {
        $urls = $this->SUT->extractFollowingCategoryUrls($html);

        $difference = array_diff($urls, $expectedUrls);
        $this->assertEmpty($difference);
    }

    public function extractCategoryUrlsProvider()
    {
        $data = array();
        $data[] = $this->examplePaginationEmpty();
        //$data[] = $this->examplePaginationOneElement();
        //$data[] = $this->examplePaginationTwoElements();

        return $data;
    }

    private function examplePaginationEmpty()
    {
        $html = '';
        $urls = array();

        return array($html, $urls);
    }

    /**
     * @dataProvider extractCategoryUrlsProvider
     */
    public function testExtractProductDetailUrlsFromCategoryPage($html, $expectedUrls)
    {
        $urls = $this->SUT->extractProductDetailUrlsFromCategoryPage($html);

        $difference = array_diff($urls, $expectedUrls);
        $this->assertEmpty($difference);
    }

    public function extractProductDetailsUrlsProvider()
    {
        $data = array();
        $data[] = $this->exampleCategoryWithoutProducts();

        return $data;
    }

    private function exampleCategoryWithoutProducts()
    {
        $html = '';
        $urls = array();

        return array($html, $urls);
    }
}
