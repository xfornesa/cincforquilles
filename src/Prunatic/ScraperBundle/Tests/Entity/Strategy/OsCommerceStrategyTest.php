<?php

namespace Prunatic\ScraperBundle\Tests\Entity\Strategy;

use Prunatic\ScraperBundle\Entity\Strategy\OsCommerceStrategy;

class OsCommerceStrategyTest extends \PHPUnit_Framework_TestCase
{
    /** @var  OsCommerceStrategy */
    protected $SUT;

    public function setUp()
    {
        $this->SUT = new OsCommerceStrategy();
    }

    public function testTypeOfGetCategoryStrategy()
    {
        $strategy = $this->SUT->getCategoryStrategy();
        $this->assertInstanceOf('\Prunatic\ScraperBundle\Entity\Strategy\Category\OsCommerceStrategy', $strategy);
    }

    public function testTypeOfGetProductStrategy()
    {
        $strategy = $this->SUT->getProductStrategy();
        $this->assertInstanceOf('\Prunatic\ScraperBundle\Entity\Strategy\Product\OsCommerceStrategy', $strategy);
    }
}
