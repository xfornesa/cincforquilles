<?php

namespace Prunatic\ScraperBundle\Tests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Goutte\Client;
use Prunatic\ScraperBundle\Entity\Scraper;
use Prunatic\ScraperBundle\Entity\Strategy\StrategyInterface;
use Prunatic\ScraperBundle\Entity\MissingStrategyException;

class ScraperTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Scraper */
    protected $SUT;

    /** @var  StrategyInterface */
    protected $strategy;

    /** @var  Client */
    protected $client;

    /** @var string  */
    protected $url;

    public function setUp()
    {
        $this->SUT = new Scraper();
        $this->strategy = $this->getMock('\Prunatic\ScraperBundle\Entity\Strategy\StrategyInterface');
        $this->client = $this->getMock('\Goutte\Client');
        $this->url = '';
    }

    /**
     * @expectedException \Prunatic\ScraperBundle\Entity\Strategy\MissingStrategyException
     */
    public function testExecuteWithoutStrategy()
    {
        $this->SUT->setHttpClient($this->client);
        $this->SUT->execute($this->url);
    }

    /**
     * @expectedException \Prunatic\ScraperBundle\Entity\Strategy\MissingHttpClientException
     */
    public function testExecuteWithoutHttpClient()
    {
        $this->SUT->setStrategy($this->strategy);
        $this->SUT->execute($this->url);
    }

    public function testExecute()
    {
        $this->strategy
            ->expects($this->once())
            ->method('retrieveProducts')
            ->with($this->equalTo(''), $this->equalTo(new ArrayCollection()))
            ->will($this->returnValue(new ArrayCollection()));

        $this->strategy
            ->expects($this->once())
            ->method('retrieveCategories')
            ->with($this->equalTo(''))
            ->will($this->returnValue(new ArrayCollection()));

        $this->SUT->setStrategy($this->strategy);
        $this->SUT->setHttpClient($this->client);

        $this->SUT->execute($this->url);
    }
}
