<?php

namespace Prunatic\WebBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Prunatic\ScraperBundle\Entity\Provider;

class LoadProviderData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $urls = array(
            array(
                'name' => 'Los utensilios del chef',
                'url' => 'http://www.losutensiliosdelchef.com/Utensilios-de-cocina.html',
                'platform' => 'oscommerce'
            ),
            array(
                'name' => 'Los utensilios de cocina',
                'url' => 'http://www.losutensiliosdecocina.es',
                'platform' => 'oscommerce'
            ),
            array(
                'name' => 'El aderezo',
                'url' => 'http://eladerezo.hola.com/tag/utensilios',
                'platform' => ''
            ),
            array(
                'name' => 'Gastronomía & Cía',
                'url' => 'http://www.gastronomiaycia.com/category/utensilios-de-cocina/',
                'platform' => ''
            )
        );
        foreach ($urls as $data) {
            $provider = new Provider();
            $provider->setName($data['name']);
            $provider->setUrl($data['url']);
            if (!empty($data['platform'])) {
                $provider->setApplicationPlatform($data['platform']);
            }
            $manager->persist($provider);
        }
        $manager->flush();
    }
}
