Feature: Search products
  In order to know more about a product
  As a visitor
  I need to be able to search for a product

  Background:
    Given I am on the Search page
      And there are following products:
      | name                                           | description                                                                                                                                                         |
      | Corta patatas - 3 cuchillas Westmark           | CORTADORA DE PATATAS PARA FREIR Y VERDURAS. OFRECE MULTIPLES POSIBILIDADES. CORTA PATATAS PARA FREIR ASI COMO VERDURAS Y FRUTAS (p.ej. PARA ENSALADAS Y MACEDONIAS) |
      | Cuchillo cebollero wusthof 16 cms ikon classic | Fabricantes de cuchillos desde 1814, esta corporación alemana radicada en Solingen es hoy uno de los líderes mundiales en herramientas de corte de acero forjado    |

  Scenario Outline: Find a products by title
     When I fill in "Query" with "<search_terms>"
      And press "Search"
     Then I should see "<product_name>"

    Examples:
        | search_terms     | product_name                                   |
        | corta patatatas  | Corta patatas - 3 cuchillas Westmark           |
        | cuchillo wusthof | Cuchillo cebollero wusthof 16 cms ikon classic |

  Scenario: Trying to find a non existent product
    When I fill in "Query" with "Non existing product"
     And press "Search"
    Then I should see "Nothing found"
