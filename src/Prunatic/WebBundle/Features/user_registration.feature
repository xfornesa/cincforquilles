Feature: User registration
  In order to keep my favorites products and interests
  As a visitor
  I need to be able to create an account in the web

  Background:
    Given there are following users:
        | email                | password |
        | existing@example.org | p@ssW0rd |
      And following interests:
        | name       |
        | Interest A |
        | Interest B |
        | Interest C |
      And I am on the Registration page

  Scenario: Successful creating account in web
    When I fill in the following:
       | Email    | example@example.org |
       | Password | p@ssW0rd            |
     And press "Register"
    Then I should be on the Homepage page
     And should see "Thank you for registering in Cinc Forquilles"

  Scenario: Successful creating account with interests
    When I fill in the following:
       | Email     | example@example.org |
       | Password  | p@ssW0rd            |
     And check "Interest A"
     And press "Register"
    Then I should be on the Homepage page
     And I expect a user exists with "example@example.org" email and "Interest A" interest

  Scenario: Trying to register with an existing email
     When I fill in the following:
        | Email    | existing@example.org |
        | Password | short                |
      And press "Register"
     Then I should be on the Registration page
      And should see "Email is already in use"

  Scenario Outline: Trying to register with a non valid email
    When I fill in the following:
       | Email    | <email>    |
       | Password | <password> |
     And press "Register"
    Then I should be on the Registration page
     And should see "<message>"

    Examples:
      | email             | password | message                                 |
      | non_valid_format  | p@ssW0rd | This value is not a valid email address |
      | wrong@example.org |          | This value should not be blank          |

  Scenario Outline: Trying to register with a non valid password
     When I fill in the following:
        | Email    | <email>    |
        | Password | <password> |
      And press "Register"
     Then I should be on the Registration page
      And should see "<message>"

    Examples:
        | email             | password      | message                                                                                                      |
        | wrong@example.org | sh0r!         | This value is too short. It should have 6 characters or more                                                 |
        | wrong@example.org |               | This value should not be blank                                                                               |
        | wrong@example.org | weak_password | This password is too weak. It should have at least one digit, one special character and 6 characters or more |
