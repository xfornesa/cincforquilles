<?php

namespace Prunatic\WebBundle\Features\Context;

require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Framework/Assert/Functions.php';

use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Gherkin\Node\TableNode;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use \PHPUnit_Framework_Assert as Assert;
use Prunatic\WebBundle\Entity\Interest;
use Prunatic\WebBundle\Entity\Product;
use Prunatic\WebBundle\Entity\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Feature context.
 */
class FeatureContext extends MinkContext implements KernelAwareInterface
{
    /** @var  KernelInterface $kernel */
    private $kernel;
    private $parameters;

    /**
     * Initializes context with parameters from behat.yml.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * Sets HttpKernel instance.
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * Retrieve the routing service
     *
     * @return \Symfony\Component\Routing\Router
     */
    protected function getRouter()
    {
        return $this->kernel->getContainer()->get('router');
    }

    /**
     * Retrieve the entity manager service
     *
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * Retrieve a User by email
     *
     * @param string $email
     * @return User
     */
    protected function findUserByEmail($email)
    {
        return $this->getEntityManager()->getRepository('PrunaticWebBundle:User')->findOneByEmail($email);
    }

    /**
     * Retrieve an Interest by name
     *
     * @param string $name
     * @return Interest
     */
    protected function findInterestByName($name)
    {
        return $this->getEntityManager()->getRepository('PrunaticWebBundle:Interest')->findOneByName($name);
    }

    /**
     * Retrieve a Product by name
     *
     * @param string $name
     * @return Product
     */
    protected function findProductByName($name)
    {
        return $this->getEntityManager()->getRepository('PrunaticWebBundle:Product')->findOneByName($name);
    }

    /**
     * @BeforeScenario
     */
    public function purgeDatabase()
    {
        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $this->getEntityManager();

        $purger = new ORMPurger($entityManager);
        $purger->purge();
    }

    /**
     * Generate page url.
     *
     * @param string $page
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return string
     */
    private function generatePageUrl($page)
    {
        $pages = array(
            'Homepage' => 'prunatic_web_homepage',
            'Registration' => 'prunatic_web_register',
            'Search' => 'prunatic_web_search'
        );
        $router = $this->getRouter();
        $url = '';
        if (isset($pages[$page])) {
            $url = $router->generate($pages[$page]);
        }
        if (empty($url)) {
            throw new NotFoundHttpException();
        }

        return $url;
    }

    /**
     * @Given /^(?:|there are )following users:$/
     */
    public function thereAreFollowingUsers(TableNode $table)
    {
        foreach ($table->getHash() as $data) {
            $this->thereIsUser($data['email'], $data['password']);
        }
    }

    /**
     * Ensure that exists a User with that parameters
     *
     * @param string $email
     * @param string $password
     * @return User
     */
    private function thereIsUser($email, $password)
    {
        $user = $this->findUserByEmail($email);
        if (empty($user)) {
            $user = new User();
            $user->setEmail($email);
            $password = $this->encodePassword($user, $password);
            $user->setPassword($password);
            $this->getEntityManager()->persist($user);
            $this->getEntityManager()->flush();
        }

        return $user;
    }

    /**
     * @Given /^(?:|there are )following interests:$/
     */
    public function thereAreFollowingInterests(TableNode $table)
    {
        foreach ($table->getHash() as $data) {
            $this->thereIsInterest($data['name']);
        }
    }

    /**
     * Ensure that exists an Interest with that name
     *
     * @param string $name
     * @return Interest
     */
    private function thereIsInterest($name)
    {
        $interest = $this->findInterestByName($name);
        if (empty($interest)) {
            $interest = new Interest();
            $interest->setName($name);
            $this->getEntityManager()->persist($interest);
            $this->getEntityManager()->flush();
        }

        return $interest;
    }


    /**
     * @Given /^(?:|there are )following products:$/
     */
    public function thereAreFollowingProducts(TableNode $table)
    {
        foreach ($table->getHash() as $data) {
            $this->thereIsProduct($data);
        }
    }

    /**
     * Ensure that exists a Product with that parameters
     *
     * @param string $data
     * @return Interest
     */
    private function thereIsProduct($data)
    {
        $product = $this->findProductByName($data['name']);
        if (empty($product)) {
            $product = new Product();
            $product->setName($data['name']);
            $product->setDescription($data['description']);
            $this->getEntityManager()->persist($product);
            $this->getEntityManager()->flush();
        }

        return $product;
    }

    /**
     * Encode a password using User defined encoder
     *
     * @param User $user
     * @param string $password
     * @return string mixed
     */
    private function encodePassword($user, $password)
    {
        $factory = $this->kernel->getContainer()->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($password, null);

        return $password;
    }

    /**
     * @Given /^I am on the (.+) page?$/
     */
    public function iAmOnThePage($page)
    {
        $url = $this->generatePageUrl($page);
        $this->getSession()->visit($url);
    }

    /**
     * @Then /^I should be on the (.+) page$/
     */
    public function iShouldBeOnThePage($page)
    {
        $url = $this->generatePageUrl($page);

        $this->assertSession()->addressEquals($url);
        Assert::assertEquals(200, $this->getSession()->getStatusCode());
    }

    /**
     * @Then /^I expect a user exists with "([^"]*)" email and "([^"]*)" interest$/
     */
    public function assertUserExistsWithInterests($userEmail, $interestName)
    {
        $user = $this->findUserByEmail($userEmail);
        $interest = $this->findInterestByName($interestName);

        Assert::assertInstanceOf('Prunatic\WebBundle\Entity\User', $user);
        Assert::assertInstanceOf('Prunatic\WebBundle\Entity\Interest', $interest);

        $userInterests = $user->getInterests();
        Assert::assertEquals(1, $userInterests->count());
        Assert::assertTrue($userInterests->contains($interest));
    }
}
