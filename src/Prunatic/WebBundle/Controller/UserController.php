<?php

namespace Prunatic\WebBundle\Controller;

use Prunatic\WebBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Prunatic\WebBundle\Validator\Constraints as PrunaticAssert;

class UserController extends Controller
{
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createRegisterForm($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $user = $form->getData();
            $password = $this->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add('success', 'Thank you for registering in Cinc Forquilles');

            return $this->redirect($this->generateUrl('prunatic_web_homepage'));
        }

        return $this->render(
            'PrunaticWebBundle:User:register.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Encode a password using User defined encoder
     *
     * @param User $user
     * @param string $password
     * @return string mixed
     */
    private function encodePassword($user, $password)
    {
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($password, null);

        return $password;
    }

    /**
     * Retrieve a form for user registration
     *
     * @param $user
     * @return \Symfony\Component\Form\Form
     */
    private function createRegisterForm($user)
    {
        $form = $this->createFormBuilder($user)
            ->add(
                'email',
                'email',
                array(
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Email(),
                        new Assert\Length(array('min' => 3)),
                    ),
                )
            )
            ->add(
                'password',
                'password',
                array(
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Length(array('min' => 6)),
                        new PrunaticAssert\PasswordStrength()
                    ),
                )
            )
            ->add(
                'interests',
                'entity',
                array(
                    'class' => 'PrunaticWebBundle:Interest',
                    'property' => 'name',
                    'expanded' => true,
                    'multiple' => true
                )
            )
            ->add('register', 'submit')
            ->getForm();

        return $form;
    }
}
