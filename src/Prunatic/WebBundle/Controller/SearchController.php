<?php

namespace Prunatic\WebBundle\Controller;

use Prunatic\WebBundle\PrunaticWebBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    public function indexAction(Request $request)
    {
        $products = array();
        $form = $this->createSearchForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $products = $this->getDoctrine()->getRepository('PrunaticWebBundle:Product')->findLikeByQueryTerms($data['query']);
        }

        return $this->render(
            'PrunaticWebBundle:Search:index.html.twig',
            array(
                'form' => $form->createView(),
                'products' => $products
            )
        );
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    private function createSearchForm()
    {
        $form = $this->createFormBuilder()
            ->add('query', 'text')
            ->add('Search', 'submit')
            ->getForm();

        return $form;
    }
}
