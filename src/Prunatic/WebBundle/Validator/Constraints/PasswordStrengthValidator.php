<?php

namespace Prunatic\WebBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PasswordStrengthValidator extends ConstraintValidator
{
    public function validate($password, Constraint $constraint)
    {
        $hasMinimumLength = false;
        $hasDigits = false;
        $hasSpecialCharacter = false;

        // check for length
        if (6 <= strlen($password)) {
            $hasMinimumLength = true;
        }

        // check for at least one digit
        if (preg_match('/\d+/', $password)) {
            $hasDigits = true;
        }

        // check for at least one special character
        if (preg_match('/.[^a-zA-Z0-9]/', $password)) {
            $hasSpecialCharacter = true;
        }

        if (!$hasMinimumLength || !$hasDigits || !$hasSpecialCharacter) {
            $this->context->addViolation($constraint->message);
        }
    }
}
