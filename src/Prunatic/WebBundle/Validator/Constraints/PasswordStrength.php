<?php

namespace Prunatic\WebBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class PasswordStrength extends Constraint
{
    public $message = 'This password is too weak. It should have at least one digit, one special character and 6 characters or more.';
}
