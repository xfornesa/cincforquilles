<?php

namespace Prunatic\WebBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Prunatic\WebBundle\Entity\Product;

class LoadProductData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        for ($i=1; $i<=10; $i++) {
            $product = new Product();
            $name = sprintf('Product num_%d', $i);
            $description = sprintf('Description num_%d', $i);
            $product->setName($name);
            $product->setDescription($description);
            $manager->persist($product);
        }
        $manager->flush();
    }
}
