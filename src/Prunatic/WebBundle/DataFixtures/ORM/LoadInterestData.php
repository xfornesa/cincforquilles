<?php

namespace Prunatic\WebBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Prunatic\WebBundle\Entity\Interest;

class LoadInterestData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        for ($i=1; $i<=10; $i++) {
            $interest = new Interest();
            $name = sprintf('Interest num %d', $i);
            $interest->setName($name);
            $manager->persist($interest);
        }
        $manager->flush();
    }
}
