<?php

namespace Prunatic\WebBundle\Tests\Entity;

use Prunatic\WebBundle\Entity\Product;
use Prunatic\WebBundle\Tests\Entity\BaseRepository;

class ProductRepositoryTest extends BaseRepository
{
    public function testFindProductByName()
    {
        $productA = $this->loadProducts();

        $queryString = 'product_a';
        $products = $this->em->getRepository('PrunaticWebBundle:Product')->findLikeByQueryTerms($queryString);
        $this->assertEquals(1, count($products));
        $this->assertContains($productA, $products);
    }

    public function testFindProductByDescription()
    {
        $productA = $this->loadProducts();

        $queryString = 'description_a';
        $products = $this->em->getRepository('PrunaticWebBundle:Product')->findLikeByQueryTerms($queryString);
        $this->assertEquals(1, count($products));
        $this->assertContains($productA, $products);
    }

    public function testTryingToFindNonExistentProduct()
    {
        $queryString = 'non_existent_term';
        $products = $this->em->getRepository('PrunaticWebBundle:Product')->findLikeByQueryTerms($queryString);
        $this->assertEmpty($products);
    }

    /**
     * @return Product
     */
    private function loadProducts()
    {
        $productA = new Product();
        $productA
            ->setName('name of the product_a')
            ->setDescription('description of the product_a with description_a');
        $this->em->persist($productA);

        $productB = new Product();
        $productB
            ->setName('name of the product_b')
            ->setDescription('description of the product_b with description_b');
        $this->em->persist($productB);

        $this->em->flush();

        return $productA;
    }
}
