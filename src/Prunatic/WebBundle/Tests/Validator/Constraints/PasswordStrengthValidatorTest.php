<?php

namespace Prunatic\WebBundle\Tests\Validator\Constraints;

use Prunatic\WebBundle\Validator\Constraints\PasswordStrength;
use Prunatic\WebBundle\Validator\Constraints\PasswordStrengthValidator;

class PasswordStrengthValidatorTest extends \PHPUnit_Framework_TestCase
{
    protected $context;
    protected $validator;

    protected function setUp()
    {
        $this->context = $this->getMock('Symfony\Component\Validator\ExecutionContext', array(), array(), '', false);
        $this->validator = new PasswordStrengthValidator();
        $this->validator->initialize($this->context);
    }

    public function testValidPassword()
    {
        $password = 'p@assw0rD';
        $this->context->expects($this->never())->method('addViolation');

        $this->validator->validate($password, new PasswordStrength());
    }

    public function testValidateMinimumLength()
    {
        $password = 'p@0rD';
        $this->context->expects($this->once())->method('addViolation');

        $this->validator->validate($password, new PasswordStrength());
    }

    public function testValidateHasDigits()
    {
        $password = 'ppppp@@@DD';
        $this->context->expects($this->once())->method('addViolation');

        $this->validator->validate($password, new PasswordStrength());
    }

    public function testValidateHasSpecialCharacters()
    {
        $password = 'ppppp12313DD';
        $this->context->expects($this->once())->method('addViolation');

        $this->validator->validate($password, new PasswordStrength());
    }
}
