
// TODO consider install FOSJsRoutingBundle to manage routes within javascript
var routes = {
    'prunatic_web_search': '#'
};

$( document ).ready(function() {
    $('#search-form').find('form').submit(function(event) {
        var form;
        form = $(this);

        // TODO To enhance UI, add a loader to get User know an action was triggered
        $.post(
            routes['prunatic_web_search'],
            form.serialize(),
            function(response) {
                $('#search-results').html(response);
            }
        );
        return false;
    });
});
